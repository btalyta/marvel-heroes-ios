//
//  HeroesTableViewCell.swift
//  Heroes
//
//  Created by Bárbara Souza on 28/02/18.
//  Copyright © 2018 Bárbara Souza. All rights reserved.
//

import UIKit
import Kingfisher

class HeroesTableViewCell: UITableViewCell {

    @IBOutlet weak var lbDescription: UILabel!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var ivThumbnail: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func prepareCell(withHero hero: Hero){
        self.lbDescription.text = hero.description
        self.lbName.text = hero.name
        if let url = URL(string: hero.thumbnail.url){
            self.ivThumbnail.kf.indicatorType = .activity
            self.ivThumbnail.kf.setImage(with: url)
        }else{
            self.ivThumbnail.image = nil
        }
        self.ivThumbnail.layer.cornerRadius = self.ivThumbnail.frame.size.height/2
        self.ivThumbnail.layer.borderWidth = 2
        self.ivThumbnail.layer.borderColor = UIColor.red.cgColor
    }
}
